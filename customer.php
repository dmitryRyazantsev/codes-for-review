<?php

/**
 * Class customer
 * @property OrdersModel $OrdersModel
 * @property OrderReserveModel $OrderReserveModel
 * @property CustomerSaleStatisticModel $CustomerSaleStatisticModel
 * @property orders_model $orders_model
 */
class customer extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('orders/OrdersModel');
		$this->load->model('orders/OrderReserveModel');
		$this->load->model('orders_model');
		$this->load->model('users/CustomerSaleStatisticModel');
		$this->load->model('catalogue/CatalogueModel');
	}

	public function sale_statistics($mode = 'day'){
		$this->benchmark->mark('code_start');

		$last_sync = get_system('customer_sale_statistics_update');

		if($mode == 'all'){
			$last_sync = '0000-00-00 00:00:00';
		}

		$this->CustomerSaleStatisticModel->addNewCustomers($last_sync);
		$updated = $this->CustomerSaleStatisticModel->updateStatistics($last_sync);

		set_system('customer_sale_statistics_update', date('Y-m-d H:i:s'));
		$this->benchmark->mark('code_end');

		echo PHP_EOL."Updated: ".$updated." customers".PHP_EOL;
		echo 'Time: '.$this->benchmark->elapsed_time('code_start', 'code_end').PHP_EOL;
		die;
	}
}
