<?php

/**
 * Class CustomerSaleStatisticModel
 * @property CataloguePriceModel $CataloguePriceModel
 */
class CustomerSaleStatisticModel extends CI_Model {

	/**
	 * Mass update users T_SALE_STATISTICS, must used only for clear table.
	 * @param $date
	 * @param int $offset
	 * @param int $step
	 * @return array
	 * @author dr
	 */
	public function updateStatistics($date = '0000-00-00 00:00:00', $offset = 0, $step = 1000){
		$users = $this->getUpdatedUser($date);
		$total = count($users);
		$updated_users = 0;

		while($updated_users < $total){
			$part = array_slice($users, $updated_users, $step, true);
			$offset = $this->update($step, $offset, array('customers' => $part));
			$updated_users += count($part);
		}

		return $updated_users;
	}

	/**
	 * Get users who have movement at orders in postman_orders by $date
	 * @param str $date
	 * @return array
	 * @author dr
	 */
	public function getUpdatedUser(str $date){
		$user_ids = array_to_assoc($this->db->select('o.user_id')
			->from(T_ORDERS_POSTMAN.' as op')
			->join(T_ORDERS.' as o', 'o.id = op.order_id')
			->where('op.last_update > "'.$date.'"')
		->group_by('o.user_id')
		->get()->result_array(), 'user_id');

		return array_keys($user_ids);
	}

	/**
	 * Add new empty customers to customer_sale_statistics.
	 * We can add users by order ids, last_updated orders if user not exist in table.
	 * @param str $date
	 * @param int $step
	 * @return int|array
	 * @example:
	 * <code>
	 * 		$this->CustomerSaleStatisticModel->addNewCustomers('2015-01-23 00:00:00');
	 * </code>
	 * @author dr
	 */
	public function addNewCustomers($date, $step = 1000){
		$users = $this->db->select('o.user_id as customer_id')
			->from(T_ORDERS_POSTMAN.' as op')
			->join(T_ORDERS.' as o', 'o.id = op.order_id')
			->join(T_SALE_STATISTICS.' as cs', 'cs.customer_id = o.user_id', 'left')
			->where('op.last_update >= "'.$date.'"')
			->where('cs.customer_id IS NULL')
			->group_by('o.user_id')
			->get()->result_array();

		$total_users = count($users);
		$inserted = 0;

		if(!$total_users){
			return array('total_users' => 0);
		}

		while($inserted < $total_users){
			$part = array_slice($users, $inserted, $step);
			$this->db->insert_batch(T_SALE_STATISTICS, $part);

			$inserted += count($part);
		}

		return array('total_users' => $total_users, 'added' => $inserted);
	}

	/**
	 * Update customer data
	 * @param int $step
	 * @param int $offset
	 * @param array $params
	 * @return int
	 * @author dr
	 */
	public function update($step = 1000, $offset = 0, $params = array()){

		foreach($params['customers'] as $customer_id){
			$genders = array(
				'0' => 'family',
				'1'	=> 'man',
				'2'	=> 'woman',
				'3'	=> 'kids',
			);

			foreach($genders as $gender_id => $gender){
					$query = "UPDATE
					  ".T_SALE_STATISTICS." as cs
					SET
					  cs.".$gender." = (
						SELECT COUNT(r.count)
						FROM ".T_CATEGORIES." as cc
						  JOIN ".T_CATEGORIES_ITEMS." as cci ON cci.cat_id=cc.id
						  JOIN ".T_RESERVE." as r ON r.cat_id=cci.item_id AND r.status != 2
						  JOIN ".T_STORAGE_DOCS." as sd ON sd.id=r.doc_id AND sd.document_type=2 AND sd.status != 2
						  JOIN ".T_ORDERS." as o ON o.id=sd.order_id
						WHERE cc.sex='".$gender_id."' AND o.user_id = cs.customer_id AND o.order_status != 16
					  ),
					  cs.".$gender."_total = (
						SELECT SUM(r.count*r.price)
						FROM ".T_CATEGORIES." as cc
						  JOIN ".T_CATEGORIES_ITEMS." as cci ON cci.cat_id=cc.id
						  JOIN ".T_RESERVE." as r ON r.cat_id=cci.item_id AND r.status != 2
						  JOIN ".T_STORAGE_DOCS." as sd ON sd.id=r.doc_id AND sd.document_type=2 AND sd.status != 2
						  JOIN ".T_ORDERS." as o ON o.id=sd.order_id
						WHERE cc.sex='".$gender_id."' AND o.user_id=cs.customer_id AND o.order_status != 16
					)
					WHERE cs.customer_id = ".$customer_id;

					$this->db->query($query);
				}

			$query = "UPDATE ".T_SALE_STATISTICS." as cs
				SET
				returns = (SELECT COUNT(*) FROM ".T_ORDERS." AS o WHERE o.order_status = 16 AND o.user_id = cs.customer_id)
				WHERE cs.customer_id = ".$customer_id;

			$this->db->query($query);
		}

		$offset += $step;

		return $offset;
	}
}
