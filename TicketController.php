<?php

class TicketController extends BaseController
{
    /**
     * Show ticket list
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function all()
    {
        if (Auth::check()) {
            $user_id = Auth::id();
            $tickets = UserTicket::where('user_id', '=', $user_id)->get();

            return View::make('frontend.profile.ticket.list')
                ->with('tickets', $tickets);
        } else {
            return Redirect::to('/profile/all', 403);
        }
    }

    /**
     * Create new ticket
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        if (Request::isMethod('post')) {
            $data       = Input::all();
            $messages   = array(
                'messages' => array(
                    array(
                        'from'      => Auth::user()->email,
                        'message'   => $data['message'],
                        'user_type' => 'user',
                        'date'      => date('H:i d.m.Y')
                    )
                )
            );

            if (Input::hasFile('attach')) {
                $documents = array();

                $name       = Input::file('attach')->getClientOriginalName();
                $ext        = Input::file('attach')->getClientOriginalExtension();
                $hash_name  = md5($name . time()) . '.' . $ext;
                
                Input::file('attach')->move('assets/user_upload/', $hash_name);

                $documents[] = array(
                    'name'  => ($name) ? $name : $hash_name,
                    'path'  => 'assets/user_upload/' . $hash_name,
                    'id'    => md5($hash_name)
                );
            }

            $data['messages']   = json_encode($messages);
            $data['status']     = UserTicket::STATUS_FROM_USER;
            $data['user_id']    = Auth::id();
            $data['documents']  = json_encode($documents);

            $ticket = new UserTicket();

            if ($ticket->create($data)) {
                return Redirect::route('frontend.user.profile.tickets');
            } else {
                return Redirect::back()->withInput($data);
            }
        } else {
            return View::make('frontend.profile.ticket.new');
        }
    }

    /**
     * Update ticket
     * @param $id
     * @return $this
     */
    public function update($id)
    {
        $ticket     = UserTicket::find($id);
        $messages   = json_decode($ticket->messages, true);
        
        $ticket->messages   = $messages;
        $ticket->documents  = json_decode($ticket->documents);

        if (Request::isMethod('post')) {
            $message = array(
                'from'      => Auth::user()->email,
                'message'   => Input::get('message'),
                'user_type' => 'user',
                'date'      => date('H:i d.m.Y')
            );
            $messages['messages'][] = $message;
            $ticket->messages       = json_encode($messages);

            if ($ticket->save()) {
                $ticket->messages = $messages;
                
                return View::make('frontend.profile.ticket.ticket')
                    ->with('ticket', $ticket);
            } else {
                return Redirect::back()->withInput();
            }
        } else {
            return View::make('frontend.profile.ticket.ticket')
                ->with('ticket', $ticket);
        }
    }

    /**
     * Make ticket closed
     * @param $id
     * @param $rate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close($id, $rate)
    {
        $ticket     = UserTicket::find($id);
        $messages   = json_decode($ticket->messages, true);
        
        $messages['rating'] = array('rate' => $rate);

        $ticket->messages   = json_encode($messages);
        $ticket->status     = UserTicket::STATUS_CLOSED;
        
        $ticket->save();
        
        return Redirect::route('frontend.user.profile.tickets');
    }
}
