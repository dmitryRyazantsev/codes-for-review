
function Calendar(){

    this.container = null;

    this.date   = new Date();
    this.year   = this.date.getFullYear();
    this.month  = this.date.getMonth();
    this.day    = this.date.getDate();
    this.today  = this.date.getDate();

    this.init = function(container){
        this.container  = container;
        this.weekend    = $('#weekend').data('weekend');
        var $this       = this;

        this.render();

        $(container).on('click', '#prev-month', function(){
            if($this.month == 0){
                $this.month = 11;
                $this.year  = $this.year - 1;
            }else{
                $this.month = $this.month - 1;
            }
            $this.render();
        });

        $(container).on('click', '#next-month', function(){
            if($this.month == 11){
                $this.month = 0;
                $this.year  = $this.year + 1;
            }else{
                $this.month = $this.month + 1;
            }
            
            $this.render();
        });

        $(container).on('click', '.prev-month.day', function(){
            return false;
        });

        $(container).on('click', '.available', function(){
            var data        = {};
            data.date       = $(this).data().year + '-' + $(this).data().month + '-' + $(this).data().day
            data.service    = $('#service_id').data('service');
            
            $.ajax({
                type: 'POST',
                url: '/booking/booking',
                data: data,
                success: function(r){
                    $('div.calendar').html(r.booking);
                }
            });
        });
    };

    this.weekDayFullNames = function(){
        var weekday = Array(7);
        weekday[1] = "Понедельник";
        weekday[2] = "Вторник";
        weekday[3] = "Среда";
        weekday[4] = "Четверг";
        weekday[5] = "Пятница";
        weekday[6] = "Суббота";
        weekday[7] = "Воскресенье";

        return weekday;
    };

    this.weekDayShortNames = function(){
        var weekday = Array(7);
        weekday[1] = "Пн";
        weekday[2] = "Вт";
        weekday[3] = "Ср";
        weekday[4] = "Чт";
        weekday[5] = "Пт";
        weekday[6] = "Сб";
        weekday[7] = "Вс";

        return weekday;
    };

    this.getDay = function(date){
        var day = date.getDay();
        
        if(day == 0){
            day = 7
        }
        
        return day - 1;
    };

    this.monthName = function(){
        var month = Array(12);
        month[0]  = "Январь";
        month[1]  = "Февраль";
        month[2]  = "Март";
        month[3]  = "Апрель";
        month[4]  = "Май";
        month[5]  = "Июнь";
        month[6]  = "Июль";
        month[7]  = "Август";
        month[8]  = "Сентябрь";
        month[9]  = "Октябрь";
        month[10] = "Ноябрь";
        month[11] = "Декабрь";

        return month;
    };

    this.render = function(){
        var head    = this.calendarHead();
        var table   = this.makeTable();
        var wrapper = $('<div>').addClass('cl-wrapper');

        wrapper.append(head);
        wrapper.append(table)

        this.container.html(wrapper);
    };

    this.calendarHead = function(){
        var div  = $('<div>').addClass('calendar-nav');
        var prev = $('<span>')
            .attr('id', 'prev-month')
            .addClass('fa fa-arrow-left');

        var next = $('<span>')
            .attr('id', 'next-month')
            .addClass('fa fa-arrow-right');

        var monthName = $('<h3>')
            .addClass('month-name')
            .text(this.monthName()[this.month] + ' ' + this.year);

        div.append(prev);
        div.append(monthName);
        div.append(next);

        return div;
    }

    this.makeTable = function(){
        var date = new Date(this.year, this.month);

        var table       = $('<table>');
        var tableHead   = this.makeTableHead();
        var tableBody   = this.makeTableBody(date);

        table.append(tableHead);
        table.append(tableBody);

        return table;
    };

    this.makeTableHead = function(){
        var dayNames    = this.weekDayShortNames();
        var tableHead   = $('<thead>');

        var tr = $('<tr>');

        for(var day in dayNames){
            
            var td = $('<td>')
                .attr('id', 'day-' + day)
                .text(dayNames[day]);
            
            if(day > 5){
                td.addClass('weekend');
            }
            
            tr.append(td);
        }

        tableHead.append(tr);

        return tableHead;
    };

    this.makeTableBody = function(date){
        var tableBody = $('<tbody>');
        var prevMonth = date;

        date.setDate(prevMonth.getDate() - this.getDay(date));

        for(var i = 0; i < 6; i++){
            var tr = $('<tr>');

            for(var j = 1; j <= 7; j++){
                var td = $('<td>');

                var day     = date.getDate();
                var month   = date.getMonth();
                var year    = date.getFullYear();

                if((day == this.today) && ( month == this.date.getMonth())) {
                    td.addClass('available').attr('id', 'today');
                }else if(date < this.date){
                    td.addClass('prev-month');
                }else if(this.month != month){
                    td.addClass('available next-month');
                }else{
                    td.addClass('available');
                }

                if(this.weekend){
                    for(var day_id in this.weekend){
                        if(date.getDay() == this.weekend[day_id])
                            td.removeClass('available').addClass('prev-month');
                    }
                }

                td.addClass('day').data({
                    'year'  : year,
                    'month' : month + 1,
                    'day'   : day
                });

                td.text(date.getDate());
                date.setDate(date.getDate() + 1);
                tr.append(td)
            }

            tableBody.append(tr);
        }

        return tableBody;
    };

    //TODO: review this method after demo
    this.booking = function(data){
        var wrapper = $('div.cl-wrapper').html('');

        for(var item in data.booking){
            var p = $('<p>').text(data.booking[item].time);
            wrapper.append(p);
        }

        return wrapper;
    }
}

(function(){
    var calendar = new Calendar;
    calendar.init($('#cal'));

})(jQuery);
